package server

import (
	"fmt"
	"mime"
	"net/http"
	"os"

	"gitlab.com/golang-utils/fs/filesystems/rootfs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/httpgzip"
)

func init() {
	// application/x-javascript
	mime.AddExtensionType(".html", "text/html; charset=utf-8")
	mime.AddExtensionType(".js", "application/javascript; charset=utf-8")
	mime.AddExtensionType(".css", "text/css; charset=utf-8")
	mime.AddExtensionType(".eot", "application/vnd.ms-fontobject")
	mime.AddExtensionType(".woff", "font/woff")
	mime.AddExtensionType(".ttf", "font/ttf")
	mime.AddExtensionType(".svg", "image/svg+xml")

	// w.Header().Set("Content-Type", "text/html; charset=utf-8")

	/*
			app.get('*.js', function(req, res, next) {
		  req.url = req.url + '.gz';
		  res.set('Content-Encoding', 'gzip');
		  res.set('Content-Type', 'text/javascript');
		  next();
		});

		// For CSS
		app.get('*.css', function(req, res, next) {
		  req.url = req.url + '.gz';
		  res.set('Content-Encoding', 'gzip');
		  res.set('Content-Type', 'text/css');
		  next();
		});
	*/
}

/*
func addHeaders(fs http.Handler) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        w.Header().Add("X-Frame-Options", "DENY")
        fs.ServeHTTP(w, r)
    }
}
*/

func MountStatic(dir path.Local, mountPoint string) error {
	if !rootfs.Exists(dir) {
		return fmt.Errorf("directory does not exist: %s", dir.ToSystem())
	}

	fs := httpgzip.FileServer(
		http.Dir(dir.ToSystem()),
		httpgzip.FileServerOptions{
			IndexHTML: true,
		},
	)

	http.Handle(mountPoint, fs)
	return nil
}

func Run(host string, port int) error {
	h := fmt.Sprintf("%s:%d", host, port)
	fmt.Printf("listening on http://%s\n", h)
	return http.ListenAndServe(h, nil)
}

func runX(host string, startingPort, port int, numberOfPortsTrying int) error {
	if port > startingPort+numberOfPortsTrying {
		return fmt.Errorf("tried more than %v ports, giving up!", numberOfPortsTrying)
	}
	err := Run(host, port)

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err.Error())
		runX(host, startingPort, port+1, numberOfPortsTrying)
	}

	return nil
}

func RunOnFreePort(host string, startingPort int, numberOfPortsTrying int) error {
	return runX(host, startingPort, startingPort, numberOfPortsTrying)
}
