package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/metakeule/static-junk-server/lib/server"
)

var (
	cfg     = config.New("static-junk-server", "a simple local fileserver")
	argPort = cfg.Int("port", "listening port (will try the follwing ten, if it is occupied)", config.Default(8080))
	argHost = cfg.String("host", "listening host", config.Default("localhost"))
	argDir  = cfg.LastDir("dir", "directory of static files (default: current directory)")
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err.Error())
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	err := cfg.Run()

	if err != nil {
		return err
	}

	err = server.MountStatic(argDir.Get(), "/")

	if err != nil {
		return err
	}

	return server.RunOnFreePort(argHost.Get(), argPort.Get(), 10)
}
